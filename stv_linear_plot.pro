#-------------------------------------------------
#
# Project created by QtCreator 2012-07-17T17:21:10
#
#-------------------------------------------------

QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = stv_linear_plot
TEMPLATE = app

SOURCES += mainwindow.cpp
SOURCES += stv_linear_plot.cpp
SOURCES += main.cpp

HEADERS += mainwindow.h
HEADERS += stv_linear_plot.h

FORMS    += mainwindow.ui

#INCLUDEPATH += /usr/local/qwt-6.1.3/include
#INCLUDEPATH += /usr/include/qwt
#LIBS += -Wl,-rpath,/usr/local/qwt-6.1.3/lib -L /usr/local/qwt-6.1.3/lib -l:libqwt.so

INCLUDEPATH += /usr/local/qwt-6.1.3-svn/include
INCLUDEPATH += /usr/include/qwt
LIBS += -Wl,-rpath,/usr/local/qwt-6.1.3-svn/lib -L /usr/local/qwt-6.1.3-svn/lib -l:libqwt.so
QMAKE_CXXFLAGS += -std=c++0x

