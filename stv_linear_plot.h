/* stv_linear_plot - Jan 2014
 * andyinyakima - Andy Laberge (andylaberge@linux.com)
 * stv_linear_plot is a rework of
 * stv090xiq by
 * UDL - Chris Lee (updatelee@gmail.com)
 * Derived from work by:
 * 	cletus2k - Taylor Jacob (rtjacob@gmail.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef STV_LINEAR_PLOT_H
#define STV_LINEAR_PLOT_H

#include <math.h>
#include <QThread>
#include <QVector>
#include <iostream>
#include <linux/dvb/frontend.h>
#include <linux/dvb/version.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string>
#include <sstream>
#include <time.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <time.h>
#include <unistd.h>

using namespace std;

class STV_plot : public QThread
{
    Q_OBJECT
signals:
    void signaldraw(QVector<double> x, QVector<double> y);
public:
    void setup(int t_adapter, int t_loop, int t_persistence, int t_magnifier);
    void sweep();
    void closeadapter();
    int view,divis;

private:
    QVector<double> x;
    QVector<double> y;
    int adapter, loop, persistence,magnifier;
    int frontend_fd;
    string frontend_devname;
    void run();
};

#endif // STV_LINEAR_PLOT_H
