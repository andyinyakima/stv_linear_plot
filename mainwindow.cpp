#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    symbols=45000;
    a_plot.view=0x00;
    a_plot.divis=500;
    ui->setupUi(this);
    qRegisterMetaType<double>("double");
    qRegisterMetaType<QVector<double> >("QVector<double>");

    connect(&a_plot, SIGNAL(signaldraw(QVector<double>, QVector<double>)), this, SLOT(qwt_draw(QVector<double>, QVector<double>)));

    curve_1 = new QwtPlotCurve("Curve 1");
    if(ui->radioButton_none->isChecked()==true)
        curve_1->setStyle(QwtPlotCurve::NoCurve);
    if(ui->radioButton_lines->isChecked()==true)
         curve_1->setStyle(QwtPlotCurve::Lines);
    if(ui->radioButton_sticks->isChecked()==true)
         curve_1->setStyle(QwtPlotCurve::Sticks);

    curve_1->setRenderHint(QwtPlotItem::RenderAntialiased, true);
    curve_1->setPen(QColor(255,0,0));
	curve_1->attach(ui->qwtPlot);

    symbol_1 = new QwtSymbol;
    symbol_1->setStyle(QwtSymbol::Ellipse);
    symbol_1->setSize(1,1);
    symbol_1->setPen(QColor(0,0,255));
    symbol_1->setBrush(QColor(0, 0, 255));
    curve_1->setSymbol(symbol_1);

    grid = new QwtPlotGrid();
    grid->enableXMin(true);
    grid->enableYMin(true);
    grid->setMajorPen(QPen(Qt::black, 0, Qt::DotLine));
    grid->setMinorPen(QPen(Qt::gray, 0 , Qt::DotLine));
    grid->attach(ui->qwtPlot);


  //  ui->qwtPlot->enableAxis(QwtPlot::yLeft ,0);

	scaleX = new QwtPlotScaleItem();
    scaleX->setAlignment(QwtScaleDraw::BottomScale);
    ui->qwtPlot->enableAxis(QwtPlot::xBottom ,1);
    //scaleX->setScaleDiv((new QwtLinearScaleEngine())->divideScale(-100,150, 10, 5));
    scaleX->attach(ui->qwtPlot);
    scaleY = new QwtPlotScaleItem();
     ui->qwtPlot->setAxisScale(QwtPlot::yLeft, -300,300);  //-150,150
  //  scaleY->setAlignment(QwtScaleDraw::LeftScale);
   // scaleY->setScaleDiv((new QwtLinearScaleEngine())->divideScale(-150,150,5,1)); //-150,150,10,5
	scaleY->attach(ui->qwtPlot);
    on_updateButton_clicked();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::qwt_draw(QVector<double> x, QVector<double> y)
{
   // int mply =2;

	QVector<double> x_1;
	QVector<double> y_1;
    QVector<double> xy_1;

	
    for (int i = 0; i < x.size(); i++) {

        x_1.append(i);
        if(ui->checkBox_IQsym->isChecked()==true)
            xy_1.append(x[i] + y[i]);
        if(ui->checkBox_Isym->isChecked()==true)
            xy_1.append(x[i]);
        if(ui->checkBox_Qsym->isChecked()==true)
            xy_1.append(y[i]);

			continue;


    }


    curve_1->setSamples(x_1, xy_1);

    ui->qwtPlot->replot();
    a_plot.setup(ui->adapterBox->currentText().toInt(), ui->loopBox->isChecked(), ui->spinBox_persistance->value(),ui->spinBox_magnifier->value());

}

void MainWindow::on_updateButton_clicked()
{
    cout << "on_pushButton_clicked()" << endl;
    a_plot.setup(ui->adapterBox->currentText().toInt(), ui->loopBox->isChecked(), ui->spinBox_persistance->value(),ui->spinBox_magnifier->value());
    ui->qwtPlot->setAxisScale(QwtPlot::xBottom, 0, symbols/ui->spinBox_magnifier->value());
    a_plot.view = ui->spinBox_hinib->value()<<4|ui->spinBox_lonib->value();
    a_plot.start();

}

void MainWindow::on_actionExit_triggered()
{
    a_plot.closeadapter();
    a_plot.quit();
    a_plot.wait();
    exit(1);
}

void MainWindow::on_spinBox_magnifier_valueChanged(int arg1)
{
    on_updateButton_clicked();
}


void MainWindow::on_spinBox_lonib_valueChanged(int arg1)
{
    int hinib = a_plot.view & 0xf0;
    a_plot.view = hinib | arg1;
    on_updateButton_clicked();
}

void MainWindow::on_spinBox_valueChanged(int arg1)
{
    int lonib = a_plot.view & 0x0f;
    a_plot.view = arg1 | lonib;
    on_updateButton_clicked();
}

void MainWindow::on_radioButton_none_clicked(bool checked)
{
    if(ui->radioButton_none->isChecked()==true)
        curve_1->setStyle(QwtPlotCurve::NoCurve);
   on_updateButton_clicked();
}

void MainWindow::on_radioButton_lines_clicked(bool checked)
{
    if(ui->radioButton_lines->isChecked()==true)
         curve_1->setStyle(QwtPlotCurve::Lines);
    on_updateButton_clicked();
}

void MainWindow::on_radioButton_sticks_clicked(bool checked)
{
    if(ui->radioButton_sticks->isChecked()==true)
         curve_1->setStyle(QwtPlotCurve::Sticks);
    on_updateButton_clicked();
}

void MainWindow::on_spinBox_div_by_valueChanged(int arg1)
{
    a_plot.divis=arg1;
    on_updateButton_clicked();
}
